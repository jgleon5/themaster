package clase1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Clase1 {

    public static void main(String[] args) {
        int datos[] = new int[5];
        int numero;
        int i = 0;
        String datoEntrada;
        InputStreamReader entrada = new InputStreamReader(System.in);
        BufferedReader flujoEntrada = new BufferedReader(entrada);

        try {
            while (i <= 4) {
                System.out.println("----------------Teclee numero------------");
                datoEntrada = flujoEntrada.readLine();
                numero = Integer.parseInt(datoEntrada);
                if (numero >= 10 && numero <= 30) {
                    System.out.println("numero correcto " + numero);
                    datos[i] = numero;
                    i = i + 1;
                } else {
                    System.out.println("numero incorrecto " + numero);
                }
            }
        } catch (IOException error) {
            System.err.println("Error " + error.getMessage());
        }
    }
}

package metodo2;

import java.util.Scanner;

public class Metodo2 {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int seleccion;
        System.out.println("seleccione la operacion 1=suma , 2=resta , 3= multiplicacion , 4 = division");
        seleccion = entrada.nextInt();
        switch (seleccion) {
            case 1:
                System.out.println("método sumar");
                sumar();
                break;
            case 2:
                System.out.println("método restar");
                restar();
                break;
            case 3:
                System.out.println("método multiplicar");
                multiplicar();
                break;
            default:
                System.out.println("método dividir");
                dividir();
                break;

        }

    }

    public static void sumar() {
        int num1 = 10;
        int num2 = 5;
        int suma = num1 + num2;
        System.out.println(suma);

    }

    public static void restar() {
        int num1 = 10;
        int num2 = 5;
        int resta = num1 - num2;
        System.out.println(resta);
    }

    public static void multiplicar() {
        int num1 = 10;
        int num2 = 5;
        int mult = num1 * num2;
        System.out.println(mult);
        
    }


    public static void dividir() {
        int num1 = 10;
        int num2 = 5;
        int division = num1 / num2;
        System.out.println(division);

    }
}

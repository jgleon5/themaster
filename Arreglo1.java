package arreglo1;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Arreglo1 {

    public static void main(String[] args) {
        InputStreamReader entrada = new InputStreamReader(System.in);
        BufferedReader flujoEntrada = new BufferedReader(entrada);
        int listado[][] = new int[3][3];
        for (int i = 0; i <= 2; i++) {
            //System.out.println("indice " + i);
            for (int j = 0; j <= 2; j++) {
                //System.out.println("indice dos " + i);
                listado[i][j] = i + j; 
            }
        }
        System.out.println("Arreglo en posición 0,2 = "+ listado[0][2]);
        System.out.println("Arreglo en posición 0,1 = "+ listado[0][1]);
        System.out.println("Arreglo en posición 2,1 = "+ listado[2][1]);
        System.out.println("Arreglo en posición 2,2 = "+ listado[2][2]);
        System.out.println("Arreglo en posición 2,3 = ERROR");
                
        for(int i = 0; i<=2; i++){
            //System.out.println("índice "+ i);
            for(int j = 0; j<=2; j++){
                //System.out.println("índice dos"+j);
                System.out.println("Arreglo en posición "+i+","+j+" = "+listado[i][j]); 
            }
        }
    }
}
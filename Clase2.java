package clase2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Clase2 {

    public static void main(String[] args) throws IOException {
        int datos[] = new int[5];
        int datos2[] = new int[5];
        int datos3[] = new int[5];
        int numero;
        int i = 0;
        int a = 0;
        String datoEntrada;
        InputStreamReader entrada = new InputStreamReader(System.in);
        BufferedReader flujoEntrada = new BufferedReader(entrada);
        System.out.println("----------------Arreglo 1------------");
        while (i <= 4) {
            datoEntrada = flujoEntrada.readLine();
            numero = Integer.parseInt(datoEntrada);
            datos[i] = numero;
            i = i + 1;

        }
        System.out.println("----------------Arreglo 2------------");
        while (a <= 4) {
            datoEntrada = flujoEntrada.readLine();
            numero = Integer.parseInt(datoEntrada);
            datos2[a] = numero;
            a = a + 1;

        }
        System.out.println("----------------Arreglo 3------------");
        datos3[0] = datos[0] + datos2[0];
        System.out.println(datos3[0]);
        datos3[1] = datos[1] + datos2[1];
        System.out.println(datos3[1]);
        datos3[2] = datos[2] + datos2[2];
        System.out.println(datos3[2]);
        datos3[3] = datos[3] + datos2[3];
        System.out.println(datos3[3]);
        datos3[4] = datos[4] + datos2[4];
        System.out.println(datos3[4]);
    }
}

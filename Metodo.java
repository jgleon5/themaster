package metodo;

public class Metodo {

    public static void main(String[] args) {
        System.out.println("Llamando a método sumar");
        sumar();
        restar();
    }

    public static void sumar() {
        int num1 = 10;
        int num2 = 5;
        int suma = num1 + num2;
        System.out.println(suma);

    }

    public static void restar() {
        int num1 = 10;
        int num2 = 5;
        int resta = num1 - num2;
        System.out.println(resta);
    }   
}

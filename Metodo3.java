package metodo3;
import java.util.Scanner;
public class Metodo3 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int seleccion;
        System.out.println("seleccione la operacion 1=suma , 2=resta , 3= multiplicacion , 4 = division");
        seleccion = entrada.nextInt();
        switch (seleccion) {
            case 1:
                System.out.println("método sumar");
                sumar();
                break;
            case 2:
                System.out.println("método restar");
                restar();
                break;
            case 3:
                System.out.println("método multiplicar");
                multiplicar();
                break;
            default:
                System.out.println("método dividir");
                dividir();
                break;
        }
    }
    public static void sumar() {
        System.out.println("ingrese un numero");
        Scanner entrada = new Scanner(System.in);
        int num1 = entrada.nextInt();
        System.out.println("ingrese un numero");
        int num2 = entrada.nextInt();
        int suma = num1 + num2;
        System.out.println("la suma es " +suma);

    }

    public static void restar() {
        System.out.println("ingrese un numero");
        Scanner entrada = new Scanner(System.in);
        int num1 = entrada.nextInt();
        System.out.println("ingrese un numero");
        int num2 = entrada.nextInt();
        int resta = num1 - num2;
        System.out.println("la resta es " +resta);
    }

    public static void multiplicar() {
        System.out.println("ingrese un numero");
        Scanner entrada = new Scanner(System.in);
        int num1 = entrada.nextInt();
        System.out.println("ingrese un numero");
        int num2 = entrada.nextInt();
        int multi = num1 * num2;
        System.out.println("la multiplicacion es " +multi);
    }

    public static void dividir() {
        System.out.println("ingrese un numero");
        Scanner entrada = new Scanner(System.in);
        int num1 = entrada.nextInt();
        System.out.println("ingrese un numero");
        int num2 = entrada.nextInt();
        int division = num1 / num2;
        System.out.println("la division es " +division);

    }
}

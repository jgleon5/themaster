package ejercicio2;

import java.util.Scanner;

public class Ejercicio2 {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        double m, cm, pul, pie, yard;
        System.out.println("ingrese el numero de metros a convertir mayores a 1 ");
        float lm = teclado.nextFloat();
        System.out.println(" metros \t" + "/" + "yardas " + "/" + "\t pulgadas " + "/\t" + " pies ");
        for (m = 1; m <= lm; m++) {
            cm = m * 100;
            pul = cm / 2.54;
            pie = pul / 12;
            yard = pie / 3;
            System.out.println(m + " / \t" + yard + " / " + pul + " / " + pie);
        }
    }
}
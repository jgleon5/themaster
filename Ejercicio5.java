package ejercicio5;

import java.util.Scanner;

public class Ejercicio5 {

    public static void main(String[] args) {
        datos();
    }

    public static void datos() {
        Scanner teclado = new Scanner(System.in);
        System.out.println("PRIMERA MATRIZ");
        int mat[][] = new int[2][2];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++) {
                System.out.println("Elemento " + i + "," + j);
                mat[i][j] = teclado.nextInt();
            }
        }
        System.out.println("SEGUNDA MATRIZ");
        int mat2[][] = new int[2][2];
        for (int i = 0; i < mat2.length; i++) {
            for (int j = 0; j < mat2.length; j++) {
                System.out.println("Elemento " + i + "," + j);
                mat2[i][j] = teclado.nextInt();
            }
        }
        System.out.println("TERCERA MATRIZ");
        int mat3[][] = new int[8][8];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                mat3[i][j] = mat[i][j] * mat2[i][j];
                System.out.println("Elemento "+ i + "," +j + " = " + mat3[i][j]);
            }
        }
    }
    public static void presentar() {

    }
}
package ejercicioclase1;

import java.util.Scanner;

public class EjercicioClase1 {

    static String cadena1 = "Loja";
    static String cadena2 = "Ecuador";
    Scanner leer = new Scanner(System.in);

    public static void main(String[] args) {
        concatenarCadenas();
        presentarLongitud();
    }

    public static void concatenarCadenas() {
        String nuevaCadena = cadena1 + cadena2;
        System.out.println(nuevaCadena);
    }

    public static void presentarLongitud() {
        int longitud1 = cadena1.length();
        int longitud2 = cadena2.length();
        System.out.println(longitud1);
        System.out.println(longitud2);
    }
}
